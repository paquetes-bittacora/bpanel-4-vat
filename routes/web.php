<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Vat\Http\Controllers\VatRateRegionsAdminController;
use Bittacora\Bpanel4\Vat\Http\Controllers\VatRatesAdminController;

Route::prefix('bpanel/tipos-iva')->name('bpanel4-vat-rates.bpanel.')
    ->middleware(['web', 'auth', 'admin-menu'])->group(function () {
        Route::get('/', [VatRatesAdminController::class, 'index'])->name('index');
        Route::get('/nuevo', [VatRatesAdminController::class, 'create'])->name('create');
        Route::post('/guardar', [VatRatesAdminController::class, 'store'])->name('store');
        Route::get('{vatRate}/editar', [VatRatesAdminController::class, 'edit'])->name('edit');
        Route::post('{vatRate}/actualizar', [VatRatesAdminController::class, 'update'])->name('update');
        Route::delete('{vatRate}/eliminar', [VatRatesAdminController::class, 'destroy'])->name('destroy');
        Route::get('{vatRate}/establecer-como-iva-envio', [VatRatesAdminController::class, 'setShippingVatRate'])->name('setShippingVatRate');
    });


Route::prefix('bpanel/regiones-tipos-iva')->name('bpanel4-vat-rates.bpanel.regions.')
    ->middleware(['web', 'auth', 'admin-menu'])->group(function () {
        Route::get('{vatRate}/anadir-region', [VatRateRegionsAdminController::class, 'create'])->name('create');
        Route::post('/anadir-region', [VatRateRegionsAdminController::class, 'store'])->name('store');
        Route::get('/{vatRateRegion}/editar-region', [VatRateRegionsAdminController::class, 'edit'])->name('edit');
        Route::post('/{vatRateRegion}/editar-region', [VatRateRegionsAdminController::class, 'update'])->name('update');
        Route::delete('/{vatRateRegion}/eliminar-region', [VatRateRegionsAdminController::class, 'destroy'])->name('destroy');
    });
