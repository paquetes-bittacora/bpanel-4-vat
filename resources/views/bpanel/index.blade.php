@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Tipos de IVA')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-vat::datatable.index') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('bpanel4-vat-rates::livewire.vat-rate-datatable', [], key('bpanel4-vat-datatable'))
        </div>
    </div>
@endsection
