<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    @livewire('form::input-text', [
        'name' => 'name',
        'labelText' => __('bpanel4-vat-rates::vat-rate.name'),
        'required'=>true,
        'value' => old('name') ?? $vatRate?->getName()
    ])
    @livewire('form::input-number', [
        'name' => 'rate',
        'labelText' => __('bpanel4-vat-rates::vat-rate.rate'),
        'required'=>true,
        'value' => old('rate') ?? $vatRate?->getRate(),
        'min' => 0, 'step' => 0.01,
        'fieldWidth' => 7,
        'labelWidth' => 3
    ])
    @livewire('form::input-checkbox', [
        'name' => 'active',
        'value' => 1,
        'labelText' => __('bpanel4-vat-rates::vat-rate.active'),
        'bpanelForm' => true,
        'checked' => $vatRate?->isActive()
    ])
    @if($vatRate !== null)
        <div class="px-4">
            <strong class="pb-3">Regiones:</strong>
            <div style="color: #777; font-size: .9rem;font-style: italic;">En este apartado puede cambiar el valor de este impuesto para distintas regiones. Por ejemplo, para el IVA general
            puede hacer que el tipo aplicado para las Islas Canarias, Ceuta y Melilla sea 0.</div>
        </div>
        <div class="px-4 pt-4">
            <a class="btn btn-primary mt-3" href="{{ route('bpanel4-vat-rates.bpanel.regions.create', ['vatRate' => $vatRate]) }}">
                <i class="fas fa-plus"></i> Añadir IVA personalizado para una región
            </a>
        </div>
    @endif
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
</form>
