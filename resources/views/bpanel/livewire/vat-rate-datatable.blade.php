<td class="align-middle">
    <div class="text-center">
        {{ $row->name }}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{ $row->rate }} %
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        @if($row->apply_to_shipping)
            <a class="btn btn-success btn-xxs mb-1" title="Activado">
                <i class="fa fa-check"></i>
            </a>
        @else
            <a class="btn btn-danger btn-xxs mb-1" title="Desactivado" href="{{ route('bpanel4-vat-rates.bpanel.setShippingVatRate', ['vatRate' => $row]) }}">
                <i class="fa fa-times"></i>
            </a>
        @endif
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-default', [
            'fieldName' => 'active',
            'model' => $row,
            'value' => $row->active,
            'size' => 'xxs'
        ], key('active-user-'.$row->id))
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', [
            'actions' => ["edit", "delete"],
            'scope' => 'bpanel4-vat-rates.bpanel',
            'model' => $row,
            'permission' => ['edit', 'delete'],
            'id' => $row->id,
            'message' => 'el tipo de iva?'
        ], key('user-buttons-'.$row->id))
    </div>
</td>
