@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-vat::form.edit-vat-rate'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-vat::form.edit-vat-rate') }}</span>
            </h4>
        </div>
        @include('bpanel4-vat-rates::bpanel._form', ['panelTitle' => __('bpanel4-vat::form.edit-vat-rate')])
    </div>

    <div class="card bcard mt-5">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-vat::form.regions') }}</span>
            </h4>
        </div>
        <div class="p-4">
            @livewire('bpanel4-vat-rates::livewire.vat-rate-region-datatable', ['vatRateId' => $vatRate->getId()])
        </div>
    </div>
@endsection
