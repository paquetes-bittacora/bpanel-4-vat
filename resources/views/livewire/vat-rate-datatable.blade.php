<td class="align-middle">
    <div class="text-center">
        {{$row->name}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{$row->reference}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{$row->size}} {{$row->unit}}
    </div>
</td>
<td class="text-center align-middle">
    @if (count($row->sizes))
        <span class="badge badge-primary">{{ count($row->sizes) }}</span></h1>
    @endif
</td>
<td class="align-middle">
    <div class="text-center">
        {{ $row->price }} €
    </div>
</td>
<td class="align-middle">
    @if(!empty($row->discounted_price))
        <div class="text-center">
            {{ $row->discounted_price }} €
        </div>
    @endif
</td>
<td class="align-middle">
    <div class="text-center">
        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d/m/Y')}}
    </div>
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-vat-rates', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el producto?'], key('user-buttons-'.$row->id))
    </div>
</td>
