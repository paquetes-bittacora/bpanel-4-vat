@livewire('form::select', [
    'name' => 'vat_rate_id',
    'allValues' => Bittacora\Bpanel4\Vat\Models\VatRate::where('active', 1)->pluck('name', 'id'),
    'labelText' => 'Tipo de IVA',
    'selectedValues' => [$model?->vat_rate_id],
    'emptyValue' => false
])
