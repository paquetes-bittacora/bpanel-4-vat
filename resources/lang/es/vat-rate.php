<?php

declare(strict_types=1);

return [
    'name' => 'Nombre',
    'rate' => 'Tipo (%)',
    'active' => 'Activado',
    'created' => 'Tipo de IVA creado',
    'deleted' => 'Tipo de IVA eliminado',
    'updated' => 'Tipo de IVA actualizado',
    'apply_to_shipping' => 'Aplicar a los gastos de envío',
    'apply_to_shipping_if_parent_is_applied' => 'Aplicar a los gastos de envío si el impuesto en el que se crea la región es aplicable a los gastos de envío',
];
