<?php

declare(strict_types=1);

return [
    'new-vat-rate' => 'Nuevo tipo de IVA',
    'edit-vat-rate' => 'Editar tipo de IVA',
    'new-vat-rate-region' => 'Nueva región para el tipo de IVA',
    'edit-vat-rate-region' => 'Editar región para el tipo de IVA',
    'regions' => 'Regiones',
];
