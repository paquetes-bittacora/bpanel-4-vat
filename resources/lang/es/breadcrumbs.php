<?php

declare(strict_types=1);

return [
    'bpanel4-vat-rates' => 'Tipos de iva',
    'bpanel' => 'Administración',
];
