# bPanel4 VAT

Módulo básico para gestionar los tipos de iva de una tienda online.

# Instalación

El paquete tiene autoinstalador, así que no debería ser necesario hacer nada, pero
si no funciona, ejecutar los siguientes comandos:

```
php artisan migrate
bpanel4-vat:install
php artisan db:seed --class="Bittacora\Bpanel4\Vat\Database\Seeders\VatRatesSeeder"
```

# Uso

Para mostrar el `<select>` de tipo de IVA en la ficha de un producto,
habrá que llamar al componente `<x-bpanel4-vat::select :model="$product"/>`.

El modelo `$product` debe usar el trait `HasVatRate`:

```php
use HasVatRate;
```

Lo único que falta es guardar el id del IVA en el campo `vat_rate_id` de la tabla
de productos, que no se crea desde este módulo ya que dependerá de cada caso
concreto.

El servicio `VatService` incluye funciones auxiliares para añadir o quitar el
iva a un precio, o para calcular el importe de IVA incluido en un precio con IVA.
Se recomienda guardar siempre en BD tanto el precio con impuestos como sin impuestos
, ya que repetir varias veces la operación de añadir/quitar iva acaba dando errores
por el redondeo. También se recomienda de forma general guardar los precios como
enteros, para evitar errores de coma flotante.
