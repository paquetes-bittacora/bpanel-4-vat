<?php

namespace Bittacora\Bpanel4\Vat\Database\Seeders;

use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Database\Seeder;

class VatRatesSeeder extends Seeder
{
    public function run(): void
    {
        VatRate::create(['name' => 'General', 'rate' => 21, 'active' => true]);
        VatRate::create(['name' => 'Reducido', 'rate' => 10, 'active' => true]);
        VatRate::create(['name' => 'Superreducido', 'rate' => 4, 'active' => true]);
        VatRate::create(['name' => 'Sin IVA', 'rate' => 0, 'active' => true]);
    }
}
