<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Database\Factories;

use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<VatRate>
 */
final class VatRateFactory extends Factory
{
    protected $model = VatRate::class;

    public function definition(): array
    {
        return [
            'name' => 'IVA',
            'rate' => 21,
            'active' => true,
        ];
    }
}
