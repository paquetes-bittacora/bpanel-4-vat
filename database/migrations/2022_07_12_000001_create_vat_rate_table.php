<?php

declare(strict_types=1);

use Illuminate\Database\Schema\Blueprint;

return new class () extends Illuminate\Database\Migrations\Migration {
    public function up(): void
    {
        Schema::create('vat_rates', static function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('rate', 5, 2, true);
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop('vat_values');
    }
};
