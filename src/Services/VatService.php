<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Services;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Contracts\ApplicableVatRate;

class VatService
{
    public function discountVatFromPrice(int $priceWithVat, ApplicableVatRate $vatRate): int
    {
        $result = $priceWithVat / (1 + $vatRate->getRate() / 100);
        return $this->roundResultToInt($result);
    }

    public function addVatToPrice(int $priceWithoutVat, ApplicableVatRate $vatRate): int
    {
        $result = $priceWithoutVat * (1 + $vatRate->getRate() / 100);
        return $this->roundResultToInt($result);
    }

    public function addVatToPriceAsObject(?Price $price, ApplicableVatRate $vatRate): ?Price
    {
        if (null === $price) {
            return null;
        }
        return Price::fromInt($this->addVatToPrice($price->toInt(), $vatRate));
    }

    /**
     * @param int $priceWithVat Precio CON IVA
     * @return int El IVA que ya estaba aplicado al precio $priceWithVat
     */
    public function getVatInPrice(int $priceWithVat, ApplicableVatRate $vatRate): int
    {
        $decimalVatRate = $vatRate->getRate() / 100;
        $result = $priceWithVat / (1 + $decimalVatRate) * $decimalVatRate;
        return $this->roundResultToInt($result);
    }

    public function getVatForPrice(int $priceWithoutVat, ApplicableVatRate $vatRate): int
    {
        $result = $priceWithoutVat * ($vatRate->getRate() / 100);
        return $this->roundResultToInt($result);
    }

    private function roundResultToInt(float|int $result): int
    {
        $result = round($result);
        return (int)$result;
    }
}
