<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Validation;

class VatRateValidator
{
    /**
     * @return array<string,string>
     */
    public function getRateCreationValidationFields(): array
    {
        return [
            'name' => 'string|max:255|required',
            'rate' => 'numeric|required|min:0',
            'active' => 'boolean',
        ];
    }

    /**
     * @return array<string, string>
     */
    public function getRateUpdateValidationFields(): array
    {
        return $this->getRateCreationValidationFields();
    }
}
