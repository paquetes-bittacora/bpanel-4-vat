<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class VatSelect extends Component
{
    public function __construct(public ?Model $model)
    {
    }

    public function render(): View
    {
        return $this->view('bpanel4-vat-rates::components.vat-rate-select');
    }
}
