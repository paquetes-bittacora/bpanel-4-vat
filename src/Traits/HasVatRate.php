<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Traits;

use Bittacora\Bpanel4\Vat\Contracts\ApplicableVatRate;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Services\VatRegionService;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;

trait HasVatRate
{
    /**
     * @return BelongsTo<VatRate, self>
     */
    public function vatRate(): BelongsTo
    {
        return $this->belongsTo(VatRate::class);
    }

    public function setVatRate(VatRate $vatRate): void
    {
        $this->vatRate()->associate($vatRate);
    }

    public function getVatRate(): ApplicableVatRate
    {
        $vatRegionService = App::make(VatRegionService::class);
        $vatRegionService->setVatRate($this->vatRate()->firstOrFail());
        return $vatRegionService->getApplicableVatRate();
    }
}
