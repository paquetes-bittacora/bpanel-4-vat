<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat;

use Bittacora\Bpanel4\Vat\Commands\InstallCommand;
use Bittacora\Bpanel4\Vat\Http\Livewire\VatRateDatatable;
use Bittacora\Bpanel4\Vat\Http\Livewire\VatRateRegionDatatable;
use Bittacora\Bpanel4\Vat\View\Components\VatSelect;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire;

class Bpanel4VatServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-vat-rates';

    public function register(): void
    {
    }

    public function boot(BladeCompiler $blade, Livewire\LivewireManager $livewire): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'bpanel4-vat');
        $this->commands([InstallCommand::class]);
        $livewire->component(
            self::PACKAGE_PREFIX . '::livewire.vat-rate-datatable',
            VatRateDatatable::class
        );
        $livewire->component(
            self::PACKAGE_PREFIX . '::livewire.vat-rate-region-datatable',
            VatRateRegionDatatable::class
        );
        $blade->component(VatSelect::class, 'bpanel4-vat::select');
    }
}
