<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Actions;

use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Database\Connection;
use Throwable;

class CreateVatRate
{
    public function __construct(private readonly Connection $db)
    {
    }

    /**
     * @param array<string, string> $data
     * @throws Throwable
     */
    public function execute(array $data): void
    {
        try {
            $this->db->beginTransaction();
            $vatRate = new VatRate();
            $vatRate->setName($data['name']);
            $vatRate->setRate((float) $data['rate']);
            $vatRate->setActive((bool) $data['active']);
            $vatRate->save();
            $this->db->commit();
        } catch (Throwable $exception) {
            $this->db->rollBack();
            throw  $exception;
        }
    }
}
