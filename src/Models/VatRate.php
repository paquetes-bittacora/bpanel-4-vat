<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Models;

use Bittacora\Bpanel4\Vat\Contracts\ApplicableVatRate;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Tipo de IVA
 *
 * @property int $id
 * @property string $name
 * @property string $rate
 * @property bool|string $active
 * @property bool|string $apply_to_shipping
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|VatRate newModelQuery()
 * @method static Builder|VatRate newQuery()
 * @method static Builder|VatRate query()
 * @method static Builder|VatRate whereActive($value)
 * @method static Builder|VatRate whereCreatedAt($value)
 * @method static Builder|VatRate whereId($value)
 * @method static Builder|VatRate whereName($value)
 * @method static Builder|VatRate whereRate($value)
 * @method static Builder|VatRate whereUpdatedAt($value)
 * @method static Collection<VatRate> all($columns = ['*'])
 * @mixin Eloquent
 */
class VatRate extends Model implements ApplicableVatRate
{
    use SoftDeletes;

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getRate(): float
    {
        return (float) $this->rate;
    }

    public function setRate(float $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @return bool|string
     */
    public function appliesToShipping(): bool
    {
        return (bool) $this->apply_to_shipping;
    }

    /**
     * @param bool|string $applyToShipping
     */
    public function setApplyToShipping(bool $applyToShipping): void
    {
        $this->apply_to_shipping = $applyToShipping ?: null;
    }
}
