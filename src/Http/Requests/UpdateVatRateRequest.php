<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Http\Requests;

use Bittacora\Bpanel4\Vat\Validation\VatRateValidator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateVatRateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(VatRateValidator $vatRateValidator): array
    {
        return $vatRateValidator->getRateUpdateValidationFields();
    }
}
