<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Http\Livewire;

use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Webmozart\Assert\Assert;

/**
 * @property int[] $selectedKeys
 */
final class VatRateDatatable extends DataTableComponent
{
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->addClass("w-30 text-center"),
            Column::make('Tipo', 'rate')->addClass("w-30 text-center"),
            Column::make('Aplicado a envío', 'apply_to_shipping	')->addClass("w-10 text-center"),
            Column::make('Activo', 'active')->addClass("w-10 text-center"),
            Column::make('Acciones')->addClass("w-10 text-center"),
        ];
    }

    /**
     * @return Builder<VatRate>
     */
    public function query(): Builder
    {
        $query = VatRate::query()
            ->orderBy('rate', 'ASC')
            ->when(
                $this->getFilter('search'),
                fn ($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
            );

        Assert::isInstanceOf($query, Builder::class);

        return $query;
    }

    public function rowView(): string
    {
        return 'bpanel4-vat-rates::bpanel.livewire.vat-rate-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            VatRate::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function setTableRowClass(string $row): string
    {
        return 'rappasoft-centered-row';
    }
}
