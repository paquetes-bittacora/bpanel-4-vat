<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Vat\Actions\CreateVatRate;
use Bittacora\Bpanel4\Vat\Actions\UpdateVatRate;
use Bittacora\Bpanel4\Vat\Http\Requests\CreateVatRateRequest;
use Bittacora\Bpanel4\Vat\Http\Requests\UpdateVatRateRequest;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\View;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use Illuminate\View\Factory;
use Throwable;

class VatRatesAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly Redirector $redirector,
        private readonly DatabaseManager $db,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    public function index(): View
    {
        $this->authorize('bpanel4-vat-rates.bpanel.index');
        return $this->view->make('bpanel4-vat-rates::bpanel.index');
    }

    public function create(): View
    {
        $this->authorize('bpanel4-vat-rates.bpanel.create');
        return $this->view->make('bpanel4-vat-rates::bpanel.create')->with([
            'action' => $this->urlGenerator->route('bpanel4-vat-rates.bpanel.store'),
            'vatRate' => null,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(CreateVatRateRequest $request, CreateVatRate $createVatRate): RedirectResponse
    {
        $this->authorize('bpanel4-vat-rates.bpanel.store');
        $createVatRate->execute($request->validated());
        return $this->redirector->route('bpanel4-vat-rates.bpanel.index')->with([
            'alert-success' => __('bpanel4-vat::vat-rate.created'),
        ]);
    }

    public function edit(VatRate $vatRate): View
    {
        $this->authorize('bpanel4-vat-rates.bpanel.edit');

        return $this->view->make('bpanel4-vat-rates::bpanel.edit')->with([
            'action' => $this->urlGenerator->route('bpanel4-vat-rates.bpanel.update', ['vatRate' => $vatRate]),
            'vatRate' => $vatRate,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(
        VatRate $vatRate,
        UpdateVatRateRequest $request,
        UpdateVatRate $updateVatRate
    ): RedirectResponse {
        $this->authorize('bpanel4-vat-rates.bpanel.update');

        $updateVatRate->execute($request->validated(), $vatRate);
        return $this->redirector->route('bpanel4-vat-rates.bpanel.index')->with([
            'alert-success' => __('bpanel4-vat::vat-rate.updated'),
        ]);
    }

    public function destroy(VatRate $vatRate): RedirectResponse
    {
        $this->authorize('bpanel4-vat-rates.bpanel.destroy');

        $vatRate->delete();

        return $this->redirector->route('bpanel4-vat-rates.bpanel.index')->with([
            'alert-success' => __('bpanel4-vat::vat-rate.deleted'),
        ]);
    }

    public function setShippingVatRate(VatRate $vatRate): RedirectResponse
    {
        $this->db->beginTransaction();
        try {
            $vatRates = VatRate::all();
            foreach ($vatRates as $existingVatRate) {
                $existingVatRate->setApplyToShipping(false);
                $existingVatRate->save();
            }
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
            $this->db->commit();
            return $this->redirector->back()->with(['alert-success' => 'IVA para los gastos de envío establecido']);
        } catch (Throwable $exception) {
            $this->exceptionHandler->report($exception);
            $this->db->rollBack();
            return $this->redirector->back()->with(['alert-danger' => 'Error al establecer el IVA de los gastos de envío']);
        }
    }
}
