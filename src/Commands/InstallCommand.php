<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Vat\Database\Seeders\VatRatesSeeder;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-vat:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete para gestionar los tipos de IVA.';
    /**
     * @var string[]
     */
    private const PERMISSIONS = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update'];

    public function handle(AdminMenu $adminMenu, Kernel $artisan): void
    {
        $this->comment('Instalando el módulo bPanel 4 VAT...');

        $this->createMenuEntries($adminMenu);
        $this->createTabs();

        $this->giveAdminPermissions();

        $artisan->call('db:seed', ['--class' => VatRatesSeeder::class, '--force' => true]);
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-vat-rates.bpanel.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-vat-rates.bpanel.index',
            'bpanel4-vat-rates.bpanel.index',
            'bpanel4-vat-rates.bpanel.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-vat-rates.bpanel.index',
            'bpanel4-vat-rates.bpanel.create',
            'bpanel4-vat-rates.bpanel.create',
            'Nuevo',
            'fa fa-plus'
        );

        Tabs::createItem(
            'bpanel4-vat-rates.bpanel.create',
            'bpanel4-vat-rates.bpanel.index',
            'bpanel4-vat-rates.bpanel.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-vat-rates.bpanel.create',
            'bpanel4-vat-rates.bpanel.create',
            'bpanel4-vat-rates.bpanel.create',
            'Nuevo',
            'fa fa-plus'
        );
    }


    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-vat-rates.bpanel',
            'Tipos de IVA',
            'fas fa-percentage'
        );
        $adminMenu->createAction(
            'bpanel4-vat-rates.bpanel',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-vat-rates.bpanel',
            'Nuevo',
            'create',
            'fas fa-plus'
        );
    }
}
