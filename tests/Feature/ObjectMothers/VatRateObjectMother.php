<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers;

use Bittacora\Bpanel4\Vat\Models\VatRate;

final class VatRateObjectMother
{
    public function createVatRate(): VatRate
    {
        $vatRate = new VatRate();
        $vatRate->setRate(21);
        $vatRate->setName('IVA');
        $vatRate->setActive(true);
        if (0 === VatRate::where('apply_to_shipping', 1)->count()) {
            $vatRate->setApplyToShipping(true);
        }
        $vatRate->save();
        return $vatRate;
    }
}
