<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Vat\Tests;

use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VatServiceTest extends TestCase
{
    use RefreshDatabase;

    private VatService $service;

    public function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(VatService::class);
    }

    /**
     * @dataProvider preciosDeEjemplo
     */
    public function testDescuentaElIvaDeUnPrecio(int $precioConIva, int $precioSinIva): void
    {
        $this->assertEquals($precioSinIva, $this->service->discountVatFromPrice($precioConIva, $this->getVatRate()));
    }

    /**
     * @dataProvider preciosDeEjemplo
     */
    public function testAnadeElIvaAUnPrecio(int $precioConIva, int $precioSinIva): void
    {
        $this->assertEquals($precioConIva, $this->service->addVatToPrice($precioSinIva, $this->getVatRate()));
    }

    public function testCalculaElImporteDeIvaAplicadoAUnProducto(): void
    {
        $this->assertEquals(161, $this->service->getVatInPrice(930, $this->getVatRate()));
    }

    public function testCalculaElIvaAAplicarParaUnaCantidad(): void
    {
        $this->assertEquals(21, $this->service->getVatForPrice(100, $this->getVatRate()));
    }

    /**
     * @return array<int,array<int,int>>
     */
    protected function preciosDeEjemplo(): array
    {
        return [
            [5432, 4489],
            [19900, 16446],
            [95000, 78512],
            [2745, 2269],
            [321, 265],
        ];
    }

    private function getVatRate(): VatRate
    {
        $vatRate = new VatRate();
        $vatRate->setName('test');
        $vatRate->setActive(true);
        $vatRate->setRate(21);
        return $vatRate;
    }
}
